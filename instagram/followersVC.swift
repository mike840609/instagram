//
//  followersVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/26.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

var show:String?
var user:String?

class followersVC: UITableViewController {
    
    var usernameArray = [String]()
    var avaArray = [PFFile]()
    
    // who we follow or who following us
    var followArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = show?.uppercaseString
        
        // load followers if tapped on followers label
        if show == "followers"{
            loadFollowers()
        }
        
        // load followings if tapped on followings label
        if show == "followings"{
            loadFollowings()
        }
        
        
    }
    
    // loading followers
    func loadFollowers(){
        
        // 找follow 類別中 following 欄位 符合 當前使用者 -> 將條件使用者 加到 followArray
        // 搜尋這些使用者的 基本資訊
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("following", equalTo: user!)
        followQuery.findObjectsInBackgroundWithBlock ({ (objects, error) in
            
            if error == nil{
                
                self.followArray.removeAll(keepCapacity: false)
                
                for obj in objects!{
                    self.followArray.append(obj.valueForKey("follower") as! String)
                }
                
                
                // find users following user
                let query = PFUser.query()
                
                // find the user information who in the followArray
                query?.whereKey(USER_NAME, containedIn: self.followArray)
                query?.addDescendingOrder("createdAt")
                query?.findObjectsInBackgroundWithBlock({ (objects, error) in
                    
                    if error == nil{
                        self.usernameArray.removeAll(keepCapacity: false)
                        self.avaArray.removeAll(keepCapacity: false)
                        
                        for obj in objects!{
                            self.usernameArray.append(obj.objectForKey(USER_NAME) as! String)
                            self.avaArray.append(obj.objectForKey("ava") as! PFFile)
                            self.tableView.reloadData()
                        }
                        
                    }else{
                        print(error?.localizedDescription)
                    }
                })
                
            }else{
                print(error?.localizedDescription)
            }
        })
        
        
    }
    
    
    
    func loadFollowings(){
        
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("follower", equalTo: user!)
        followQuery.findObjectsInBackgroundWithBlock ({ (objects, error) in
            if error == nil{
                self.followArray.removeAll(keepCapacity: false)
                
                // 把追蹤名單加到陣列
                for obj in objects!{
                    self.followArray.append(obj.valueForKey("following") as! String)
                }
                
                // 搜尋使用者資訊
                //                let query = PFQuery(className: "_User")
                let query = PFUser.query()!
                query.whereKey(USER_NAME, containedIn: self.followArray)
                query.addDescendingOrder("createdAt")
                query.findObjectsInBackgroundWithBlock({ (objects, error) in
                    if error == nil{
                        self.usernameArray.removeAll(keepCapacity: false)
                        self.avaArray.removeAll(keepCapacity: false)
                        
                        for obj in objects!{
                            
                            
                            self.usernameArray.append(obj.objectForKey(USER_NAME) as! String)
                            self.avaArray.append(obj.objectForKey("ava") as! PFFile)
                            self.tableView.reloadData()
                        }
                        
                    }else{
                        print(error!.localizedDescription)
                    }
                })
                
            }else{
                print(error!.localizedDescription)
            }
        })
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usernameArray.count
    }
    
    // cell height
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //  320 / 4 = 80
        return self.view.frame.size.width / 4
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Config Cell
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! followersCell
        cell.usernameLbl.text = usernameArray[indexPath.row]
        avaArray[indexPath.row].getDataInBackgroundWithBlock { (data, error) in
            if error == nil{
                cell.avaImg.image = UIImage(data: data!)
            }else{
                print(error?.localizedDescription)
            }
        }
        
        // 判斷追蹤,追蹤按鈕變色
        let query = PFQuery(className: "follow")
        query.whereKey("follower", equalTo: PFUser.currentUser()!.username!)
        query.whereKey("following", equalTo: cell.usernameLbl.text!)
        query.countObjectsInBackgroundWithBlock ({ (count, error) in
            if error == nil{
                if count == 0 {
                    cell.followBtn.setTitle("FOLLOW", forState: .Normal)
                    cell.followBtn.backgroundColor = .lightGrayColor()
                }else{
                    cell.followBtn.setTitle("FOLLOWING", forState: .Normal)
                    cell.followBtn.backgroundColor = .greenColor()
                }
                
            }
        })
        
        // hide follow button for current user
        if cell.usernameLbl.text == PFUser.currentUser()?.username{
            cell.followBtn.hidden = true
        }
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // recall cell to call further cell's data
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! followersCell
        
        // 判斷欄位是否是使用者本身
        if cell.usernameLbl.text! == PFUser.currentUser()?.username!{
            
            let home = self.storyboard?.instantiateViewControllerWithIdentifier("homeVC") as! homeVC
            self.navigationController?.pushViewController(home, animated: true)
            
        }else{
            
            guestname.append(cell.usernameLbl.text!)
            let guest = self.storyboard?.instantiateViewControllerWithIdentifier("guestVC") as! guestVC
            self.navigationController?.pushViewController(guest, animated: true)
            
        }
        
    }
    
    
    
}
