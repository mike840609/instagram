//
//  picturCell.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/26.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit

class picturCell: UICollectionViewCell {
    
    @IBOutlet weak var picImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // aligement
        let width = UIScreen.mainScreen().bounds.width
        picImg.frame = CGRectMake(0, 0, width / 3, width / 3)
    }
}
