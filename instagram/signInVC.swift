//
//  signInVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/24.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class signInVC: UIViewController {
    
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var usernameTxt: MateriaTextField!
    @IBOutlet weak var passwordTxt: MateriaTextField!
    
    
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var forgotBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // text color
        label.textColor = UIColor.whiteColor()
        
        // aligement
        label.frame = CGRectMake(10, 80, self.view.frame.size.width - 20, 50)
        usernameTxt.frame = CGRectMake(10, label.frame.origin.y + 70, self.view.frame.size.width - 20, 30)
        passwordTxt.frame = CGRectMake(10, usernameTxt.frame.origin.y + 40, self.view.frame.size.width - 20, 30)
        
        forgotBtn.frame = CGRectMake(10, passwordTxt.frame.origin.y + 30, self.view.frame.size.width - 20, 30)
        signInBtn.frame = CGRectMake(20, forgotBtn.frame.origin.y + 40, self.view.frame.size.width / 4, 30)
        signUpBtn.frame = CGRectMake(self.view.frame.size.width - self.view.frame.size.width / 4 - 20, signInBtn.frame.origin.y, self.view.frame.size.width / 4, 30)
     
        
        // background 
        let bg = UIImageView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        bg.image = UIImage(named: "Background")
        bg.layer.zPosition = -1 
        self.view.addSubview(bg)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - IBAction
    
    @IBAction func signInBtn_click(sender: AnyObject) {
        print(#function)
        
        // hide keyboard
        self.view.endEditing(true)
        
        if usernameTxt.text!.isEmpty || passwordTxt.text!.isEmpty{
            
            let alert = UIAlertController(title: "Please", message: "Please fill all fields", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
        // login function
        PFUser.logInWithUsernameInBackground(usernameTxt.text!, password: passwordTxt.text!) { (user, error) in
            
            if error == nil{
                
                NSUserDefaults.standardUserDefaults().setObject(user!.username, forKey: USER_NAME)
                NSUserDefaults.standardUserDefaults().synchronize()
                
                let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.login()
                
            }else{
                
                let alert = UIAlertController(title: "Error", message: error!.localizedDescription, preferredStyle: .Alert)
                let ok = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)

                
            }
        }
        
        
    }
    
}
