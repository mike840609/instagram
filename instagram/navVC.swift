//
//  navVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/4.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit

class navVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title color
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        
        // button tint color
        self.navigationBar.tintColor = .whiteColor()
        
        // backgroung color
        self.navigationBar.barTintColor = UIColor(red: 18.0/255.0, green: 86.0/255.0, blue: 136.0/255.0, alpha: 1)
        
        // 透明度
        self.navigationBar.translucent = true
        
    }
    
    // white status bar function
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }


}
