//
//  commentVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/6.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit

var commentuuid  = [String]()
var commentowner  = [String]()


class commentVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    var refresh = UIRefreshControl()
    
    var tableViewHeight : CGFloat = 0
    var commentY:CGFloat = 0
    var commentHeight: CGFloat = 0
    
    
    // preload func
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
        
        // call keyboard
        commentTxt.becomeFirstResponder()
        
    }
    
    // postload func
    override func viewWillDisappear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    

}
