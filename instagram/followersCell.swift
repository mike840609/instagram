//
//  followersCell.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/26.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class followersCell: UITableViewCell {
    
    @IBOutlet weak var avaImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // alignment
        let width = UIScreen.mainScreen().bounds.width
        
        avaImg.frame = CGRectMake(10, 10, width/5.3, width/5.3)
        usernameLbl.frame = CGRectMake(avaImg.frame.size.width + 20, 30, width/3.2, 30)
        followBtn.frame = CGRectMake(width - width/3.5 - 10, 30, width/3.5, 30)
        
//        avaImg.layer.cornerRadius = avaImg.frame.size.width / 2
//        avaImg.clipsToBounds = true
    }
    
    
    @IBAction func followBtn_click(sender: AnyObject) {
        
        let title = followBtn.titleForState(.Normal)
        
        // 追蹤
        if title == "FOLLOW"{
            let object = PFObject(className: "follow")
            object["follower"] = PFUser.currentUser()?.username
            object["following"] = usernameLbl.text
            object.saveInBackgroundWithBlock({ (success, error) in
                if success{
                    self.followBtn.setTitle("FOLLOWING", forState: .Normal)
                    self.followBtn.backgroundColor = .greenColor()
                }else{
                    print(error?.localizedDescription)
                }
            })
            // 取消追蹤
        }else{
            let query = PFQuery(className: "follow")
            query.whereKey("follower", equalTo: PFUser.currentUser()!.username!)
            query.whereKey("following", equalTo: usernameLbl.text!)
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                if error == nil{
                    
                    for obj in objects!{
                        obj.deleteInBackgroundWithBlock({ (success, error) in
                            if success{
                                self.followBtn.setTitle("FOLLOW", forState: .Normal)
                                self.followBtn.backgroundColor = .lightGrayColor()
                                
                            }else{
                                print(error?.localizedDescription)
                            }
                        })
                        
                    }
                }else{
                    print(error?.localizedDescription)
                }
            })
            
        }
        
    }
}
