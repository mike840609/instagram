//
//  Extension.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/2.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import Foundation
import UIKit

//  MARK: - Autolayout
extension UIView{
    
    // 不限定傳入參數個數
    func addConstraintWithFormat(format:String,views:UIView...){
        
        var viewsDictionary = [String:UIView]()
        
        for (index,view) in views.enumerate(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
}