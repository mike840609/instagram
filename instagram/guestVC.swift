//
//  guestVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/27.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

var guestname = [String]()

class guestVC: UICollectionViewController {

    // ui
    var refresher: UIRefreshControl!
    var page: Int = 10
    
    // hold data from server
    var uuidArray = [String]()
    var picArray = [PFFile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.backgroundColor = .whiteColor()
        
        // top title
        self.navigationItem.title = guestname.last
        
        // new back button 
        self.navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(title: "back", style: .Plain, target: self, action: #selector(guestVC.back(_:)))
        self.navigationItem.leftBarButtonItem = backBtn
        
        // swipe to go back
        let backSwipe = UISwipeGestureRecognizer(target: self, action:#selector(guestVC.back))
        backSwipe.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(backSwipe)
        
        // pull to refresh
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(guestVC.refresh), forControlEvents: .ValueChanged)
        collectionView?.addSubview(refresher)
        
        // call load posts func
        loadPosts()
    }
    
    
    // back function
    func back(sender: UIBarButtonItem) {
        
        // push back 
        self.navigationController?.popViewControllerAnimated(true)
        
        //clean guest username or ddeduct the last guest username from guestname = Array
        if !guestname.isEmpty{
            guestname.removeLast()
        }
    }
    
    // refresh function
    func refresh() {
        collectionView?.reloadData()
        refresher.endRefreshing()
    }
    
    // load post
    func loadPosts(){
        
        let query = PFQuery(className: "posts")
        query.whereKey(USER_NAME, equalTo: guestname.last!)
        query.limit = page
        query.findObjectsInBackgroundWithBlock ({ (objects, error) in
            if error == nil{
                
                // clean up
                self.uuidArray.removeAll(keepCapacity: false)
                self.picArray.removeAll(keepCapacity: false)
                
                for obj in objects!{
                    self.uuidArray.append(obj.valueForKey("uuid") as! String)
                    self.picArray.append(obj.valueForKey("pic") as! PFFile)
                }
                self.collectionView?.reloadData()
            }else{
                print(error?.localizedDescription)
            }
        })
        
    }
    func loadMore(){
        
        // 當前紀錄比總比數小
        if page <= picArray.count{
            
            // 更新紀錄
            page += 12
            
            let query = PFQuery(className: "posts")
            query.whereKey(USER_NAME, equalTo: guestname.last!)
            query.limit = page
            
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                if error == nil{
                    
                    // clean up
                    self.uuidArray.removeAll(keepCapacity: false)
                    self.picArray.removeAll(keepCapacity: false)
                    
                    //find related objects
                    for obj in objects!{
                        self.uuidArray.append(obj.valueForKey("uuid") as! String)
                        self.picArray.append(obj.valueForKey("pic") as! PFFile)
                    }
                    
                    print("loaded + \(self.page)")
                    
                    self.collectionView?.reloadData()
                    
                }else{
                    print(error?.localizedDescription)
                }
            })
        }
        
    }
    
    
    // MARK: - ScrollViewDeleegate
    
    // load more while scrolling down
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height{
            self.loadMore()
        }
    }
    
    
    // MARK: - CollectionView Delegate & Datasource

    // cell num
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }

    // Customer func - cell size
    func collectionView(collectionView: UICollectionView, layout collectionViewLayOut:UICollectionViewLayout , sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
        
        let size = CGSize(width: self.view.frame.width/3, height:  self.view.frame.width/3)
        return size
        
    }

    
    // cell config
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! picturCell
        
        //connect data from array
        picArray[indexPath.row].getDataInBackgroundWithBlock ({ (data, error) in
            if error == nil{
                cell.picImg.image = UIImage(data: data!)
            }else{
                print(error?.localizedDescription)
            }
        })
        return cell
    }
    
    // header View
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", forIndexPath: indexPath) as! headerView
        
        
        // STEP 1  LOAD DATA OF GUEST ===================================================
        let infoQuery = PFQuery(className: "_User")
        infoQuery.whereKey("username", equalTo: guestname.last!)
        infoQuery.findObjectsInBackgroundWithBlock ({ (objects, error) in
            if error == nil{
                
                // show wrong user
                if objects == nil{
                    print("wrong user")
                }
                
                for obj in objects!{
                    header.fullnameLbl.text = obj.objectForKey("fullname") as? String
                    header.bioLbl.text = obj.objectForKey("bio") as? String
                    header.bioLbl.sizeToFit()
                    header.webTxt.text = obj.objectForKey("web") as? String
                    header.webTxt.sizeToFit()
                    
                    let avaFile:PFFile = (obj.objectForKey("ava") as? PFFile)!
                    avaFile.getDataInBackgroundWithBlock({ (data, error) in
                        header.avaImg.image = UIImage(data: data!)
                    })
                }
                
            }else{
                print(error?.localizedDescription)
            }
        })
        
        // STEP 2 SHOW DO CURRENT USER FOLLOW GUEST OR DO NOT ===================================================
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("follower", equalTo: PFUser.currentUser()!.username!)
        followQuery.whereKey("following", equalTo: guestname.last!)
        followQuery.countObjectsInBackgroundWithBlock( { (count, error) in
            
            if error == nil{
                if count == 0{
                    header.editBtn.setTitle("FOLLOW", forState: .Normal)
                    header.editBtn.backgroundColor = .lightGrayColor()
                }else{
                    header.editBtn.setTitle("FOLLOWING", forState: .Normal)
                    header.editBtn.backgroundColor = .greenColor()
                    
                }
            }else{
                print(error?.localizedDescription)
            }
        })
    
        // STEP 3 Count statics ===================================================
        
        // count posts
        let posts = PFQuery(className: "posts")
        posts.whereKey(USER_NAME, equalTo: guestname.last!)
        posts.countObjectsInBackgroundWithBlock ({ (count, error) in
            if error == nil{
                header.posts.text = "\(count)"
            }else{
                print(error?.localizedDescription)
            }
        })
        // count followers
        let followers = PFQuery(className: "follow")
        followers.whereKey("following", equalTo: guestname.last!)
        followers.countObjectsInBackgroundWithBlock ({ (count, error) in
            if error == nil{
                header.followers.text = "\(count)"
            }else{
                print(error?.localizedDescription)
            }
        })
        
        // count followings
        let followings = PFQuery(className: "follow")
        followings.whereKey("follower", equalTo: guestname.last!)
        followings.countObjectsInBackgroundWithBlock ({ (count, error) in
            if error == nil{
                header.followings.text = "\(count)"
            }else{
                print(error?.localizedDescription)
            }
        })
        
        
        // STEP 4 Implement tap gestures ===================================================
        
        //tap to posts
        let postsTap = UITapGestureRecognizer(target: self, action: #selector(guestVC.postssTap))
        postsTap.numberOfTapsRequired = 1
        header.posts.userInteractionEnabled = true
        header.posts.addGestureRecognizer(postsTap)
        
        // tap followers
        let followersTap = UITapGestureRecognizer(target: self, action: #selector(guestVC.followersTap))
        followersTap.numberOfTapsRequired = 1
        header.followers.userInteractionEnabled = true
        header.followers.addGestureRecognizer(followersTap)
        
        //tap followings
        let followingsTap = UITapGestureRecognizer(target: self, action: #selector(guestVC.followingsTap))
        followingsTap.numberOfTapsRequired = 1
        header.followers.userInteractionEnabled = true
        header.followers.addGestureRecognizer(followingsTap)
        
        return header
        
    }
    
    // MARK: - Customer function
    
    // 回到最上方
    func postssTap(){
        if !picArray.isEmpty{
            let index = NSIndexPath(forItem: 3, inSection: 0)
            self.collectionView?.scrollToItemAtIndexPath(index, atScrollPosition: .Top, animated: true)
        }
    }
    
    // 進到 guest 的粉絲列表
    func followersTap(){
        
        user = guestname.last!
        show = "followers"
        
        let followers = self.storyboard?.instantiateViewControllerWithIdentifier("followersVC") as! followersVC
        self.navigationController?.pushViewController(followers, animated: true)
    }
    
    // 進到 guest 的追蹤中列表
    func followingsTap(){
        user = guestname.last!
        show = "followings"
        
        let followings = self.storyboard?.instantiateViewControllerWithIdentifier("followersVC") as! followersVC
        self.navigationController?.pushViewController(followings, animated: true)
    }
    

}