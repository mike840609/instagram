//
//  tabVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/4.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit

class tabVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // color of item
        self.tabBar.tintColor = .whiteColor()
        
        // color of background
        self.tabBar.barTintColor = UIColor(red: 37 / 255.0, green: 39/255.0, blue: 42/255.0, alpha: 1)
        
        self.tabBar.translucent = false
        
        
        
    }



}
