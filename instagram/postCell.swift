//
//  postCell.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/1.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class postCell: UITableViewCell {
    
    
    @IBOutlet weak var avaImg: UIImageView!
    @IBOutlet weak var usernameBtn: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var picImg: UIImageView!
    
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var moreBtn: UIButton!
    
    
    @IBOutlet weak var likeLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var uuidLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

        // clear like button title color
        likeBtn.setTitleColor(UIColor.clearColor(), forState: .Normal)
        
        // double tap to like
        let likeTap = UITapGestureRecognizer(target: self, action: #selector(postCell.likeTap))
        likeTap.numberOfTapsRequired = 2
        picImg.userInteractionEnabled = true
        picImg.addGestureRecognizer(likeTap)
        
        
//        let width = UIScreen.mainScreen().bounds.width
//        let pictureWidth = width - 20
//        
//        // 設定佈局
//        
//        avaImg.translatesAutoresizingMaskIntoConstraints = false
//        usernameBtn.translatesAutoresizingMaskIntoConstraints = false
//        dateLbl.translatesAutoresizingMaskIntoConstraints = false
//        
//        picImg.translatesAutoresizingMaskIntoConstraints = false
//        
//        likeBtn.translatesAutoresizingMaskIntoConstraints = false
//        commentBtn.translatesAutoresizingMaskIntoConstraints = false
//        moreBtn.translatesAutoresizingMaskIntoConstraints = false
//        
//        likeLbl.translatesAutoresizingMaskIntoConstraints = false
//        titleLbl.translatesAutoresizingMaskIntoConstraints = false
//        uuidLbl.translatesAutoresizingMaskIntoConstraints = false
//        
//        
//        // 添加autolayout
//        addConstraintWithFormat("V:|-10-[v0(30)]-10-[v1(\(pictureWidth))]-5-[v2(30)]", views: avaImg,picImg,likeBtn)
//        addConstraintWithFormat("V:|-10-[v0]", views: usernameBtn)
//        addConstraintWithFormat("V:[v0]-10-[v1]", views: picImg,commentBtn)
//        addConstraintWithFormat("V:|-10-[v0]", views: dateLbl)
//        addConstraintWithFormat("V:[v0]-5-[v1]-5-|", views: likeBtn,titleLbl)
//        addConstraintWithFormat("V:[v0]-5-[v1]", views: picImg,moreBtn)
//        addConstraintWithFormat("V:[v0]-10-[v1]", views: picImg,likeLbl)
//        
//        
//        addConstraintWithFormat("H:|-10-[v0(30)]-10-[v1]-10-|", views: avaImg,usernameBtn)
//        addConstraintWithFormat("H:|-10-[v0]-10-|", views: picImg)
//        addConstraintWithFormat("H:|-15-[v0(30)]-10-[v1]-20-[v2]", views: likeBtn,likeLbl,commentBtn)
//        addConstraintWithFormat("H:[v0]-15-|", views: moreBtn)
//        addConstraintWithFormat("H:|-15-[v0]-15-|", views: titleLbl)
//        addConstraintWithFormat("H:|[v0]-10-|", views: dateLbl)
        
        avaImg.layer.cornerRadius = avaImg.frame.size.width / 2
        avaImg.clipsToBounds = true
    
    }
    
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    // double tap to like
    func likeTap() {
        
        // create large like gray heart
        let likePic = UIImageView(image: UIImage(named: "unlike.png"))
        likePic.frame.size.width = picImg.frame.size.width / 1.5
        likePic.frame.size.height = picImg.frame.size.width / 1.5
        likePic.center = picImg.center
        likePic.alpha = 0.8
        self.addSubview(likePic)
        
        // hide likePic with animation and transform to be smaller
        UIView.animateWithDuration(0.8) { () -> Void in
            likePic.alpha = 0
            likePic.transform = CGAffineTransformMakeScale(0.1, 0.1)
        }
        
        // declare title of button
        let title = likeBtn.titleForState(.Normal)
        
        if title == "unlike" {
            
            let object = PFObject(className: "likes")
            object["by"] = PFUser.currentUser()?.username
            object["to"] = uuidLbl.text
            object.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                if success {
                    print("liked")
                    self.likeBtn.setTitle("like", forState: .Normal)
                    self.likeBtn.setBackgroundImage(UIImage(named: "like.png"), forState: .Normal)
                    
                    // send notification if we liked to refresh TableView
                    NSNotificationCenter.defaultCenter().postNotificationName("liked", object: nil)
                    
                    
                }
            })
            
        }
        
    }
    
    

    @IBAction func likeBtn_click(sender: AnyObject) {
        
        // declare title of button
         let title = sender.titleForState(.Normal)
        
        // 按讚
        if title == "unlike"{
            
            let object = PFObject(className: "likes")
            object["by"] = PFUser.currentUser()?.username
            object["to"] = uuidLbl.text
            object.saveInBackgroundWithBlock({ (success, error) in
                if success{
                    print("liked")
                    self.likeBtn.setTitle("like", forState: .Normal)
                    self.likeBtn.setBackgroundImage(UIImage(named:"like.png"), forState: .Normal)
                    
                    
                    // 寄送推播通知
                    NSNotificationCenter.defaultCenter().postNotificationName("liked", object: nil)
                }
            })
            
        }else{
            // 收回讚
            let query = PFQuery(className: "likse")
            query.whereKey("by", equalTo: PFUser.currentUser()!.username!)
            query.whereKey("to", equalTo: uuidLbl.text!)
            
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                
                
                for obj in objects!{
                    
                    obj.deleteInBackgroundWithBlock({ (success, error) in
                        
                        if success{
                            
                            print("disliked")
                            
                            self.likeBtn.setTitle("unlike", forState: .Normal)
                            self.likeBtn.setBackgroundImage(UIImage(named:"unlike.png"), forState: .Normal)
                            
                            // 寄送推播通知
                            NSNotificationCenter.defaultCenter().postNotificationName("liked", object: nil)
                        }
                    })
                }
                
            })
            
            
        }
        
    }
    
    
}