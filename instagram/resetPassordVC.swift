//
//  resetPassordVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/24.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class resetPassordVC: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTxt.frame = CGRectMake(10, 120, self.view.frame.size.width - 20, 30)
        resetBtn.frame = CGRectMake(20, emailTxt.frame.origin.y + 50 , self.view.frame.size.width / 4 , 30)
        cancelBtn.frame = CGRectMake(self.view.frame.size.width - self.view.frame.size.width / 4 - 20, resetBtn.frame.origin.y , self.view.frame.size.width / 4 , 30)


        // tap to gide keyboard
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        hideTap.numberOfTapsRequired = 1
        self.view.userInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
        
        // background
        let bg = UIImageView(frame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height))
        bg.image = UIImage(named: "Background")
        bg.layer.zPosition = -1
        self.view.addSubview(bg)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func hideKeyboard(recognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
    }

    @IBAction func resetBtn_click(sender: AnyObject) {
     
        // hide keyboard
        self.view.endEditing(true)
        
        if emailTxt.text!.isEmpty{
            
            let alert = UIAlertController(title: "Email Field", message: "Please enter the email", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alert.addAction(ok)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        // request for requesting password
        
        PFUser.requestPasswordResetForEmailInBackground(emailTxt.text!) { (success, error) in
            
            if success{
                
                let alert = UIAlertController(title: "Email for reseting password  ", message: "has been sent the reset email , please check your email inbox", preferredStyle: .Alert)
                
                // if pressed OK call self .dismiss  func
                let ok = UIAlertAction(title: "OK", style: .Default, handler: {(UIAlertAction) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                
                alert.addAction(ok)
                self.presentViewController(alert, animated: true, completion: nil)

            }else{
             print(error?.localizedDescription)
            }
        }
    }
    
    
    @IBAction func cancelBtn_click(sender: AnyObject) {
        
        // hide key board
        self.view.endEditing(true)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }


}
