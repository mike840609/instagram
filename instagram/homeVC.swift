//
//  homeVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/26.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class homeVC: UICollectionViewController {
    
    // refresher variable
    var refresher:UIRefreshControl!
    
    // size of page
    var page:Int = 10
    
    var uuidArray = [String]()
    var picArray = [PFFile]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // alaways vertical
        self.collectionView?.alwaysBounceVertical = true
        
        
        // background color
        collectionView?.backgroundColor = .whiteColor()
        
        // Navigation Title
        self.navigationItem.title = PFUser.currentUser()?.username?.uppercaseString
        
        // pull to refresh
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(homeVC.refresh), forControlEvents: UIControlEvents.ValueChanged)
        collectionView?.addSubview(refresher)
        
        // receive notification from editVC=====================================
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.reload), name: "reload", object: nil)
        
        
        // receive notification from uploadVC=====================================
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.uploaded), name: "uploaded", object: nil)
        
        // load post
        loadPosts()
    }
    
    func refresh(){
        
        collectionView?.reloadData()
        refresher.endRefreshing()
    }
    
    func reload(notification:NSNotification){
        collectionView?.reloadData()
    }
    
    func uploaded(notification:NSNotification){
        loadPosts()
    }
    
    func loadPosts(){
        
        let query = PFQuery(className: "posts")
        query.whereKey(USER_NAME, equalTo: PFUser.currentUser()!.username!)
        query.limit = page
        query.findObjectsInBackgroundWithBlock ({ (objects, error) in
            
            if error == nil{
                
                // clean up
                self.uuidArray.removeAll(keepCapacity: false)
                self.picArray.removeAll(keepCapacity: false)
                
                // find objects related to our request
                for object in objects!{
                    self.uuidArray.append(object.valueForKey("uuid") as! String)
                    self.picArray.append(object.valueForKey("pic") as! PFFile)
                }
                self.collectionView?.reloadData()
                
            }else{
                print(error?.localizedDescription)
            }
        })
        
    }
    
    func loadMore(){
        
        // 當前紀錄比總比數小
        if page <= picArray.count{
            
            // 更新紀錄
            page += 12
            
            let query = PFQuery(className: "posts")
            query.whereKey(USER_NAME, equalTo: PFUser.currentUser()!.username!)
            query.limit = page
            
            query.findObjectsInBackgroundWithBlock({ (objects, error) in
                if error == nil{
                    
                    // clean up
                    self.uuidArray.removeAll(keepCapacity: false)
                    self.picArray.removeAll(keepCapacity: false)
                    
                    //find related objects
                    for obj in objects!{
                        self.uuidArray.append(obj.valueForKey("uuid") as! String)
                        self.picArray.append(obj.valueForKey("pic") as! PFFile)
                    }
                    
                    
                    self.collectionView?.reloadData()
                    
                }else{
                    print(error?.localizedDescription)
                }
            })
        }
        
    }
    
    
    // MARK: - ScrollViewDeleegate
    
    // load more while scrolling down
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height{
            self.loadMore()
        }
    }
    
    // MARK: - UICollectionViewDataSource & Delegate
    
    
    //  header config
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        
        // define header
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", forIndexPath: indexPath) as! headerView
        
        // STEP 1 USER DATA
        // get user data with connections to collums of PFUser class
        header.fullnameLbl.text = (PFUser.currentUser()?.objectForKey("fullname") as? String)?.uppercaseString
        header.webTxt.text = PFUser.currentUser()?.objectForKey("web") as? String
        header.webTxt.sizeToFit()
        header.bioLbl.text = PFUser.currentUser()?.objectForKey("bio") as? String
        header.bioLbl.sizeToFit()
        header.editBtn.setTitle("edit profile", forState: .Normal)
        
        
        
        // STEP 2 COUNT STATISTICS
        let avaQuery = PFUser.currentUser()?.objectForKey("ava") as! PFFile
        avaQuery.getDataInBackgroundWithBlock { (data, error) in
            header.avaImg.image = UIImage(data: data!)
        }
        
        // count total posts
        let posts = PFQuery(className: "posts")
        posts.whereKey("username", equalTo: PFUser.currentUser()!.username!)
        posts.countObjectsInBackgroundWithBlock { (count, error) in
            if error == nil{
                header.posts.text = "\(count)"
            }
        }
        
        // count total followers
        let followers = PFQuery(className: "follow")
        followers.whereKey("following", equalTo: PFUser.currentUser()!.username!)
        followers.countObjectsInBackgroundWithBlock { (count, error) in
            if error == nil{
                header.followers.text = "\(count)"
            }
        }
        
        // count total following
        let followings = PFQuery(className: "follow")
        followings.whereKey("follower", equalTo: PFUser.currentUser()!.username!)
        followings.countObjectsInBackgroundWithBlock { (count, error) in
            if error == nil {
                header.followings.text = "\(count)"
            }
        }
        
        
        
        // STEP 3 IMPLEMENT TAP GESTURE
        // tap post
        let postsTap = UITapGestureRecognizer(target: self, action: #selector(homeVC.postsTap))
        postsTap.numberOfTapsRequired = 1
        header.posts.userInteractionEnabled = true
        header.posts.addGestureRecognizer(postsTap)
        
        // tap followers
        let followersTap = UITapGestureRecognizer(target: self, action: #selector(homeVC.followersTap))
        followersTap.numberOfTapsRequired = 1
        header.followers.userInteractionEnabled = true
        header.followers.addGestureRecognizer(followersTap)
        
        
        // tap following
        let followingTap = UITapGestureRecognizer(target: self, action: #selector(homeVC.followingTap))
        followingTap.numberOfTapsRequired = 1
        header.followings.userInteractionEnabled = true
        header.followings.addGestureRecognizer(followingTap)
        
        return header
    }
    
    // cell numb
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }
    
    
    // Customer func - cell size
    func collectionView(collectionView: UICollectionView, layout collectionViewLayOut:UICollectionViewLayout , sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
        
        let size = CGSize(width: self.view.frame.width/3, height:  self.view.frame.width/3)
        return size
        
    }
    
    
    // config cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // define cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! picturCell
        
        // get picture from the picArray
        picArray[indexPath.row].getDataInBackgroundWithBlock { (data:NSData?, error:NSError?) -> Void in
            if error == nil {
                cell.picImg.image = UIImage(data: data!)
            }
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        // 把當前的 uuid 串到陣列尾巴
        postuuid.append(uuidArray[indexPath.row])
        
        // navigate 到 post view controller
        let post = self.storyboard?.instantiateViewControllerWithIdentifier("postVC") as! postVC
        self.navigationController?.pushViewController(post, animated: true)
        
    }
    
    // MARK: - Customer func
    func postsTap(){
        
        if !picArray.isEmpty{
            let index = NSIndexPath(forItem: 0, inSection: 0)
            self.collectionView?.scrollToItemAtIndexPath(index, atScrollPosition: UICollectionViewScrollPosition.Top, animated: true)
            
        }
    }
    
    
    
    func followersTap(){
        
        user = PFUser.currentUser()!.username!
        show = "followers"
        
        // instance storyboard
        let followers = self.storyboard?.instantiateViewControllerWithIdentifier("followersVC") as! followersVC
        
        // present
        self.navigationController?.pushViewController(followers, animated: true)
    }
    
    func followingTap(){
        
        user = PFUser.currentUser()!.username!
        show = "followings"
        
        // instance storyboard
        let followings = self.storyboard?.instantiateViewControllerWithIdentifier("followersVC") as! followersVC
        
        // present
        self.navigationController?.pushViewController(followings, animated: true)
        
    }
    
    // MARK: - IBAction
    
    @IBAction func logout(sender: AnyObject) {
        PFUser.logOutInBackgroundWithBlock { (error) in
            if error == nil{
                
                // remove logged user from memory
                NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_NAME)
                NSUserDefaults.standardUserDefaults().synchronize()
                
                let signin = self.storyboard?.instantiateViewControllerWithIdentifier("signInVC") as! signInVC
                let appDelegate : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.window?.rootViewController = signin
            }
        }
    }
    

    
    
    
}
