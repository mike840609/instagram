//
//  postVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/7/1.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

var postuuid = [String]()

class postVC: UITableViewController {

    // arrays to hold information from server
    var avaArray = [PFFile]()
    var usernameArray = [String]()
    var dateArray = [NSDate?]()
    var picArray = [PFFile]()
    var uuidArray = [String]()
    var titleArray = [String]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "NewsFeed"
        
        // new back button
        self.navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(title: "back", style: .Plain, target: self, action: #selector(postVC.back))
        self.navigationItem.leftBarButtonItem = backBtn
        
        // 監聽廣播通知 更新案讚事件
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(refresh), name:"liked", object: nil)
        
        
        // swipe to go back
        let backSwipe = UISwipeGestureRecognizer(target: self, action: #selector(postVC.back))
        backSwipe.direction = .Right
        self.view.addGestureRecognizer(backSwipe)
        
        // dynamic cell height
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 520
        
        
        // find post
        let postQuery = PFQuery(className: "posts")
        postQuery.whereKey("uuid", equalTo: postuuid.last!)
        postQuery.findObjectsInBackgroundWithBlock ({ (objects, error) in
            if error == nil{
                
                // clean up
                self.avaArray.removeAll(keepCapacity:false)
                self.usernameArray.removeAll(keepCapacity:false)
                self.dateArray.removeAll(keepCapacity:false)
                self.picArray.removeAll(keepCapacity:false)
                self.uuidArray.removeAll(keepCapacity:false)
                self.titleArray.removeAll(keepCapacity:false)
                
                // find related object
                for obj in objects!{
                    self.avaArray.append(obj.valueForKey("ava") as! PFFile)
                    self.usernameArray.append(obj.valueForKey("username") as! String)
                    self.dateArray.append(obj.createdAt)
                    self.picArray.append(obj.valueForKey("pic") as! PFFile)
                    self.uuidArray.append(obj.valueForKey("uuid") as! String)
                    self.titleArray.append(obj.valueForKey("title") as! String)
                }
                
             self.tableView.reloadData()
                
            }
        })
        
    }
    
    // MARK: - CUSTOMER FUNCTION
    func back(sender: UIButton){
        
        // 退到上一層 root Controller
        self.navigationController?.popViewControllerAnimated(true)
        
        // 清理 uuid 陣列
        if !postuuid.isEmpty{
            
            // remove end element
            postuuid.removeLast()
        }
    }
    
    func refresh()  {
        self.tableView.reloadData()
    }


    // MARK: - Table view data source


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return picArray.count
    }
    
    
    // config cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as! postCell
        
        cell.usernameBtn.setTitle(usernameArray[indexPath.row], forState: .Normal)
        cell.usernameBtn.sizeToFit()
        cell.uuidLbl.text = uuidArray[indexPath.row]
        cell.titleLbl.text = titleArray[indexPath.row]
        cell.titleLbl.sizeToFit()
        
        avaArray[indexPath.row].getDataInBackgroundWithBlock { (data, error) in
            cell.avaImg.image = UIImage(data: data!)
        }
        
        picArray[indexPath.row].getDataInBackgroundWithBlock { (data, error) in
            cell.picImg.image = UIImage(data: data!)
        }
        
        // calculate post date ==========================================================================
        let from = dateArray[indexPath.row]
        let now = NSDate()
        let components : NSCalendarUnit = [.Second, .Minute, .Hour, .Day, .WeekOfMonth]
        let difference = NSCalendar.currentCalendar().components(components, fromDate: from!, toDate: now, options: [])
        
        if difference.second <= 0 {
            cell.dateLbl.text = "just now"
        }
        if difference.second > 0 && difference.minute == 0 {
            cell.dateLbl.text = "\(difference.second) 秒前."
        }
        if difference.minute > 0 && difference.hour == 0 {
            cell.dateLbl.text = "\(difference.minute) 分鐘前."
        }
        if difference.hour > 0 && difference.day == 0 {
            cell.dateLbl.text = "\(difference.hour) 小時前."
        }
        if difference.day > 0 && difference.weekOfMonth == 0 {
            cell.dateLbl.text = "\(difference.day) 天前."
        }
        if difference.weekOfMonth > 0 {
            cell.dateLbl.text = "\(difference.weekOfMonth) 週前"
        }
        
        //=================================================================================================
        
        // 判斷user 是否按過like
        let didlike = PFQuery(className: "likes")
        didlike.whereKey("by", equalTo: PFUser.currentUser()!.username!)
        didlike.whereKey("to", equalTo: cell.uuidLbl.text!)
        didlike.countObjectsInBackgroundWithBlock { (count, error) in
            // 計數等於零 表示使用者沒按過讚
            if count == 0 {
                cell.likeBtn.setTitle("unlike", forState: .Normal)
                cell.likeBtn.setBackgroundImage(UIImage(named: "unlike.png"), forState: .Normal)
            }else{
                cell.likeBtn.setTitle("like", forState: .Normal)
                cell.likeBtn.setBackgroundImage(UIImage(named: "like.png"), forState: .Normal)
            }
        }
        
        // 總讚數
        let countLikes = PFQuery(className: "likes")
        countLikes.whereKey("to", equalTo: cell.uuidLbl.text!)
        countLikes.countObjectsInBackgroundWithBlock { (count, error) in
            cell.likeLbl.text = "\(count)"
        }
        
        // 給予編號 ===================================================== 可用來識別cell的好用方法
        cell.usernameBtn.layer.setValue(indexPath, forKey: "index")
        
        cell.commentBtn.layer.setValue(indexPath, forKey: "index")
        
        return cell
    }

    
    @IBAction func usernameBtn_click(sender: AnyObject) {
        
        // call index of button
        let i = sender.layer.valueForKey("index") as! NSIndexPath
        
        // call cell to call further cell data
        let cell = tableView.cellForRowAtIndexPath(i) as! postCell
        
        // if user tapped on himself go home, else go guest
        if cell.usernameBtn.titleLabel?.text == PFUser.currentUser()?.username {
            let home = self.storyboard?.instantiateViewControllerWithIdentifier("homeVC") as! homeVC
            self.navigationController?.pushViewController(home, animated: true)
        } else {
            guestname.append(cell.usernameBtn.titleLabel!.text!)
            let guest = self.storyboard?.instantiateViewControllerWithIdentifier("guestVC") as! guestVC
            self.navigationController?.pushViewController(guest, animated: true)
        }
        
        
    }
    @IBAction func commentBtn_click(sender: AnyObject) {
        
        // 獲得 button index
        let i = sender.layer.valueForKey("index") as! NSIndexPath
        
        let cell = tableView.cellForRowAtIndexPath(i) as! postCell
        
        // 全域變數 添加當前資料
        commentuuid.append(cell.uuidLbl.text!)
        commentowner.append(cell.usernameBtn.titleLabel!.text!)
        
        // 前往 commentVC
        let comment = self.storyboard?.instantiateViewControllerWithIdentifier("commentVC") as! commentVC
        self.navigationController?.pushViewController(comment, animated: true)
        
    }
    
}
