//
//  uploadVC.swift
//  instagram
//
//  Created by 蔡鈞 on 2016/6/30.
//  Copyright © 2016年 蔡鈞. All rights reserved.
//

import UIKit
import Parse

class uploadVC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var picImg: UIImageView!
    @IBOutlet weak var titleTxt: UITextView!
    @IBOutlet weak var publishBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alignment()
        
        // hide button
        removeBtn.hidden = true
        
        // UserInterface initialize
        picImg.image = UIImage(named: "cloud-computing")
        
        
        
        // disable publish btn
        publishBtn.enabled = false
        publishBtn.backgroundColor = .lightGrayColor()
        
        // hide keyboard
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(uploadVC.hideKeyboard))
        hideTap.numberOfTapsRequired = 1
        self.view.userInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
        
        // select image tap
        let picTap = UITapGestureRecognizer(target: self, action: #selector(uploadVC.selectImg))
        picTap.numberOfTapsRequired = 1
        picImg.userInteractionEnabled = true
        picImg.addGestureRecognizer(picTap)
        
        
    }
    

    func alignment(){
        
        let width = self.view.frame.size.width
        
        picImg.frame = CGRectMake(15, self.navigationController!.navigationBar.frame.size.height + 35 , width/4.5, width/4.5)
        titleTxt.frame = CGRectMake(picImg.frame.size.width + 25, picImg.frame.origin.y, width - titleTxt.frame.origin.x - 20, picImg.frame.size.height)
        
        removeBtn.frame = CGRectMake(picImg.frame.origin.x, picImg.frame.origin.y + picImg.frame.size.height , picImg.frame.size.width, 30)
        
        
    }
    
    func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    func selectImg(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .PhotoLibrary
        picker.allowsEditing = true
        presentViewController(picker, animated: true, completion: nil)
        
    }
    
    // MARK: - ImagePickerControllerDelegate
    
    // hold selected Img
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picImg.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
        
        // enable publish btn
        publishBtn.enabled = true
        publishBtn.backgroundColor = UIColor(red: 0/255.0, green: 177.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        // 顯示隱藏按鈕
        removeBtn.hidden = false
        
        // implement second tap for zooming image
        let zoomTap = UITapGestureRecognizer(target: self, action: #selector(uploadVC.zoomImg))
        zoomTap.numberOfTapsRequired = 1
        picImg.userInteractionEnabled = true
        picImg.addGestureRecognizer(zoomTap)
    }
    
    func zoomImg(){
        
        // define frame of zoomed image
        let zoomed = CGRectMake(0, self.view.center.y - self.view.center.x, self.view.frame.size.width, self.view.frame.size.width)
        
        // frame of unzoomed(small) image
        let unzoomed = CGRectMake(15,self.navigationController!.navigationBar.frame.size.height + 35, self.view.frame.size.width / 4.5 , self.view.frame.size.width / 4.5)
        
        // if picture is unzoomed zoomed it
        if picImg.frame == unzoomed{
            UIView.animateWithDuration(0.3, animations: {
                self.picImg.frame = zoomed
                
                // hide object from background
                self.view.backgroundColor = .blackColor()
                self.titleTxt.alpha = 0
                self.publishBtn.alpha = 0
            })
        }else{
            // with animation 
            UIView.animateWithDuration(0.3, animations: { 
                self.picImg.frame = unzoomed
                
                self.view.backgroundColor = .whiteColor()
                self.titleTxt.alpha = 1
                self.publishBtn.alpha = 1

            })
        }
        
        
    }
    
    @IBAction func publishBtn_clicked(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        let object = PFObject(className: "posts")
        object[USER_NAME] = PFUser.currentUser()!.username
        object["ava"] = PFUser.currentUser()?.valueForKey("ava") as! PFFile
        
        // nuique id
        object["uuid"] = "\(PFUser.currentUser()!.username)\(NSUUID().UUIDString)"
        print("UUID string: \(PFUser.currentUser()!.username)\(NSUUID().UUIDString)")
        
        if titleTxt.text.isEmpty{
            object["title"] = ""
        }else{
            // 保留字串完整資訊
            object["title"] = titleTxt.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        }
        
        let imageData = UIImageJPEGRepresentation(picImg.image!, 0.5)
        let imageFile = PFFile(name: "post.jpg", data: imageData!)
        object["pic"] = imageFile
        
        object.saveInBackgroundWithBlock ({ (success, error) in
            if error == nil{
                
                // 寄送推播通知 讓其他 Controller 收到通知
                NSNotificationCenter.defaultCenter().postNotificationName("uploaded", object: nil)
                
                // switch to another viewController at 0 index of tabber
                self.tabBarController!.selectedIndex = 0
                
                // reset everything
                self.viewDidLoad()
                self.titleTxt.text = ""
            }
        })
    }
    
    

    @IBAction func removeBtn_click(sender: AnyObject) {
        self.viewDidLoad()
    }
    
}
